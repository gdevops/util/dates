"""attente.py

Calcul du nombre de jours et de semaines jusqu'au 1er avril 2027


Exemple d'appel
==================

::

    python attente --depuis 2023-05-01


::

	python attente_retraite.py --depuis 2024-07-05
	Date de retraite:jeudi 01 avril 2027
	Aujourd'hui vendredi 05 juillet 2024 il reste encore 2 ans 8 mois 3 semaines 6 jours à attendre !
	c'est à dire encore 1000 jours à attendre !


"""

import sys
import pendulum

DATE_RETRAITE = pendulum.datetime(2027, 4, 1)
DATE_RETRAITE_STR = DATE_RETRAITE.format(
    "dddd DD MMMM YYYY", locale="fr"
)


def set_depuis(depuis: pendulum.DateTime = pendulum.now()):
    depuis_str = depuis.format("dddd DD MMMM YYYY", locale="fr")
    # calcul de la durée
    duree = DATE_RETRAITE - depuis
    duree_str = duree.in_words(locale="fr")  # formattage de la durée en français
    print(
        f"Date de retraite:{DATE_RETRAITE_STR}\n"
        f"Aujourd'hui {depuis_str} il reste encore {duree_str} à attendre !\n"
        f"c'est à dire encore {duree.in_days()} jours à attendre !"
    )


class Options:
    """Les options

    --help
        pour avoir de l'aide

    --depuis YYYY-MM-DD
        Introduire la date YYYY-MM-DD

    Exemple d'appels
    =================

    python attente.py --depuis YYYY-MM-DD


    """

    def __init__(self):
        self.depuis = pendulum.now()

    def __str__(self):
        chaine = "Options:\n\n- depuis: %s\n- nbdays: %s\n" % (self.depuis,)
        return chaine



if __name__ == "__main__":
    """Point d'entrée du script Python."""
    options = Options()
    for i, argument in enumerate(sys.argv):
        if argument == "--depuis":
            options.depuis = pendulum.from_format(sys.argv[i + 1], "YYYY-MM-DD")
        elif argument == "--help":
            print(Options.__doc__)
            exit(0)

    set_depuis(options.depuis)
