"""durees_jina_amini.py

Calcul du nombre de jours et de semaines depuis l'assassinat de Jina Amini
le vendredi 16 septembre 2022.


Exemple d'appel
==================

::

    python durees_jina_amini.py --limit 2022-12-03



"""

import sys
import pendulum

ASSASSINAT_DE_JINA_AMINI = pendulum.datetime(2022, 9, 16)
ASSASSINAT_DE_JINA_AMINI_STR = ASSASSINAT_DE_JINA_AMINI.format(
    "dddd DD MMMM YYYY", locale="fr"
)


def set_limit(limit: pendulum.DateTime):
    limit_str = limit.format("dddd DD MMMM YYYY", locale="fr")
    # calcul de la durée
    duree = limit - ASSASSINAT_DE_JINA_AMINI
    duree_str = duree.in_words(locale="fr")  # formattage de la durée en français
    print(
        f"Jina Amini est morte le {ASSASSINAT_DE_JINA_AMINI_STR} N°semaine:{ASSASSINAT_DE_JINA_AMINI.isocalendar().week} ({ASSASSINAT_DE_JINA_AMINI.isocalendar().year})\n"
        f"ce {limit_str} cela fait donc {duree_str}\n"
        f"=> {duree.in_days()}e jour"
        f" = on est dans la {duree.in_weeks()+1}e semaine de la révolution, N°semaine:{limit.isocalendar().week} ({limit.isocalendar().year})"
    )


class Options:
    """Les options

    --help
        pour avoir de l'aide

    --limit YYYY-MM-DD
        Introduire la date YYYY-MM-DD

    --nbdays <nbdays>
       Pour avoir la liste des durées sur n jours


    Exemple d'appels
    =================

    python durees_jina_amini.py --limit YYYY-MM-DD


    """

    def __init__(self):
        self.limit = pendulum.now()
        self.nbdays = None

    def __str__(self):
        chaine = "Options:\n\n- limit: %s\n- nbdays: %s\n" % (self.limit,)
        return chaine


def trt_liste_days(nbdays: int):
    current_day = ASSASSINAT_DE_JINA_AMINI.add(days=1)
    indice = 1
    while indice <= nbdays:
        current_day_str = current_day.format("dddd DD MMMM YYYY", locale="fr")
        # calcul de la durée
        duree = current_day - ASSASSINAT_DE_JINA_AMINI
        duree_str = duree.in_words(locale="fr")  # formattage de la durée en français
        print(
            f"{indice}e jour {current_day_str} "
            f"=> {duree.in_weeks()+1}e semaine de la révolution; N°semaine:{current_day.isocalendar().week}-{current_day.isocalendar().year}"
        )
        current_day = current_day.add(days=1)
        indice = indice + 1


def trt_duree(options):
    if options.nbdays is None:
        set_limit(options.limit)
    else:
        trt_liste_days(options.nbdays)


if __name__ == "__main__":
    """Point d'entrée du script Python."""
    options = Options()
    for i, argument in enumerate(sys.argv):
        if argument == "--limit":
            options.limit = pendulum.from_format(sys.argv[i + 1], "YYYY-MM-DD")
        if argument == "--nbdays":
            options.nbdays = int(sys.argv[i + 1])
        elif argument == "--help":
            print(Options.__doc__)
            exit(0)

    trt_duree(options)
