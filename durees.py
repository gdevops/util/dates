"""durees_jina_amini.py

Calcul du nombre de jours et de semaines depuis l'assassinat de Jina Amini
le vendredi 16 septembre 2022.


Exemple d'appel
==================

::

    python durees.py  --from 2023-01-07 --to 2023-03-09

::

    Durée:3 mois 3 semaines 1 jour => Nb jours:113 Nb months:3 Nb weeks:16

"""

import sys
import pendulum

def set_to(options):
    to_str = options.to.format("dddd DD MMMM YYYY", locale="fr")
    # calcul de la durée
    duree = options.to - options.from_date
    duree_str = duree.in_words(locale="fr")  # formattage de la durée en français
    print(f"Durée:{duree_str} => Nb jours:{duree.days} Nb months:{duree.months} Nb weeks:{int(duree.total_weeks())}")


class Options:
    """Les options

    --help
        pour avoir de l'aide

    --from YYYY-MM-DD
        Introduire la date YYYY-MM-DD

    --to YYYY-MM-DD
        Introduire la date YYYY-MM-DD

    --nbdays <nbdays>
       Pour avoir la liste des durées sur n jours


    Exemple d'appels
    =================

    python durees.py  --from 2022-09-16 --to 2023-01-07
    python durees.py  --from 2022-09-16  --nbdays 100


    """

    def __init__(self):
        self.limit = pendulum.now()
        self.nbdays = None

    def __str__(self):
        chaine = f"Options:\n\n- from:{self.from_date}\n- to: {self.to}\n- nbdays: {self.nbdays}\n"
        return chaine


def trt_liste_days(options):
    current_day = options.from_date.add(days=1)
    indice = 1
    while indice <= options.nbdays:
        current_day_str = current_day.format("dddd DD MMMM YYYY", locale="fr")
        # calcul de la durée
        duree = current_day - options.from_date
        duree_str = duree.in_words(locale="fr")  # formattage de la durée en français
        print(
            f"{indice}e jour {current_day_str} "
            f"=> N°semaine:{current_day.isocalendar().week}-{current_day.isocalendar().year}"
        )
        current_day = current_day.add(days=1)
        indice = indice + 1


def trt_duree(options):
    if options.nbdays is None:
        set_to(options)
    else:
        trt_liste_days(options)


if __name__ == "__main__":
    """Point d'entrée du script Python."""
    options = Options()
    for i, argument in enumerate(sys.argv):
        if argument == "--from":
            options.from_date = pendulum.from_format(sys.argv[i + 1], "YYYY-MM-DD")
        elif argument == "--to":
            options.to = pendulum.from_format(sys.argv[i + 1], "YYYY-MM-DD")
        elif argument == "--nbdays":
            options.nbdays = int(sys.argv[i + 1])
        elif argument == "--help":
            print(Options.__doc__)
            exit(0)

    trt_duree(options)
