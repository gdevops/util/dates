"""attente.py

Calcul du nombre de jours et de semaines jusqu'au 20 janvier 2025


Exemple d'appel
==================

::

    python attente_20_janvier_2025 --depuis 2024-01-20


::

	python attente_20_janvier_2025 --depuis 2024-01-20


"""

import sys
import pendulum

DATE_20_JANVIER_2025 = pendulum.datetime(2025, 1, 20)
DATE_20_JANVIER_2025_STR = DATE_20_JANVIER_2025.format(
    "dddd DD MMMM YYYY", locale="fr"
)


def set_depuis(depuis: pendulum.DateTime = pendulum.now()):
    depuis_str = depuis.format("dddd DD MMMM YYYY", locale="fr")
    # calcul de la durée
    duree = DATE_20_JANVIER_2025 - depuis
    duree_str = duree.in_words(locale="fr")  # formattage de la durée en français
    print(
        f"{duree.in_days()} jours avant l'arrivée des monstres le {DATE_20_JANVIER_2025_STR}\n"
        f"Aujourd'hui {depuis_str} il reste {duree_str} !\n"
    )


class Options:
    """Les options

    --help
        pour avoir de l'aide

    --depuis YYYY-MM-DD
        Introduire la date YYYY-MM-DD

    Exemple d'appels
    =================

    python attente.py --depuis YYYY-MM-DD


    """

    def __init__(self):
        self.depuis = pendulum.now()

    def __str__(self):
        chaine = "Options:\n\n- depuis: %s\n- nbdays: %s\n" % (self.depuis,)
        return chaine



if __name__ == "__main__":
    """Point d'entrée du script Python."""
    options = Options()
    for i, argument in enumerate(sys.argv):
        if argument == "--depuis":
            options.depuis = pendulum.from_format(sys.argv[i + 1], "YYYY-MM-DD")
        elif argument == "--help":
            print(Options.__doc__)
            exit(0)

    set_depuis(options.depuis)
